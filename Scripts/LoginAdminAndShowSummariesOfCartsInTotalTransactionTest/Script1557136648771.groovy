import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFctory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.85.211.179:8085/')

WebUI.setText(findTestObject('Object Repository/LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/input_Username_username'), 
    'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/input_Password_password'), 
    'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('Object Repository/LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/a_Total Transaction'), 
    'Total Transaction')

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_transactionId1'), 
    td_tansactionId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_product1'), 
    td_product)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_amountId1'), 
    td_amountId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_transactionId2'), 
    td_transactionId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_product2'), 
    td_product)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_amountId2'), 
    td_amountId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_transactionId3'), 
    td_transactionId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_product3'), 
    td_product)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_amountId3'), 
    td_amountId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_transactionId4'), 
    td_transactionId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_product4'), 
    td_product)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_amountId4'), 
    td_amountId)

WebUI.verifyElementText(findTestObject('LoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_totalPrice'), 
    td_totalPrice)
WebDriver driver = DriverFctory.getWebDriver()

WebElement Table = driver.findElement(By.xpath('/html/body/app-root/app-order-list/div/div/table/tbody'))

'To local rows of table it will Capture all the rows available in the table '
List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

WebUI.verifyEqual(4,Rows.size())

WebUI.closeBrowser()

