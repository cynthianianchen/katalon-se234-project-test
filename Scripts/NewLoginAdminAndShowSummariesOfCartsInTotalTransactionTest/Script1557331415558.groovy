import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.85.211.179:8085/')

WebUI.setText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/button_Login'))

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/a_Total Transaction'), 'Total Transaction')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_Banana Garden Banana Rambutan'), 
    'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_Garden Banana Orange Papaya Rambutan'), 
    'Garden, Banana, Orange, Papaya, Rambutan')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_Garden Banana Orange'), 'Garden, Banana, Orange')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_20120 THB'), '20,120 THB')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_60570 THB'), '60,570 THB')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_460000 THB'), '460,000 THB')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_40924 THB'), '40,924 THB')

WebUI.verifyElementText(findTestObject('NewLoginAdminAndShowSummariesOfCartsInTotalTransaction/Page_ProjectBackend/td_60580 THB'), '60,580 THB')

WebUI.closeBrowser()

