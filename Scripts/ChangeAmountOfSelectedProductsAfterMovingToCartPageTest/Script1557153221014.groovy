import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.85.211.179:8085/')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Username_username'), 
    'user')

WebUI.setEncryptedText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Password_password'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/button_add to cart'))

WebUI.click(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/a_Carts            5'))

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/td_Garden'), 
    'Garden')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/td_Banana'), 
    'Banana')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/td_Orange'), 
    'Orange')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/td_Papaya'), 
    'Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/td_Rambutan'), 
    'Rambutan')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/div_Total price  20462 THB'), 
    'Total price: 20,462 THB')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Garden_amount'), 
    '2')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Banana_amount'), 
    '3')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Orange_amount'), 
    '2')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Papaya_amount'), 
    '4')

WebUI.setText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/input_Rambutan_amount'), 
    '3')

WebUI.verifyElementText(findTestObject('Object Repository/ChangeAmountOfSelectedProductsAfterMovingToCartPage/Page_ProjectBackend/p_Total price  41118 THB'), 
    'Total price: 61,236 THB')

WebUI.closeBrowser()

